package ua.danit.fs11;

import java.util.Arrays;
import java.util.Objects;

public class HappyFamily {

    public static class Pet {
        private String species = "cat";
        private String nickname = "Murka";
        private int age = 1;
        private int trickLevel = 50;
        private String[] habits = {"to play", "to lie", "to run"};

        private void eat() {
            System.out.println("Я кушаю!");
        }

        private void respond() {
            System.out.println("Привет, хозяин. Я - " + nickname + ". Я соскучился!");
        }

        private void foul() {
            System.out.println("Нужно хорошо замести следы...");
        }

        @Override
        public String toString() {
            return species + "{nickname='" + nickname
                    + "', age=" + age
                    + ", trickLevel=" + trickLevel
                    + ", habits=[" + habits[0]
                    + ", " + habits[1]
                    + ", " + habits[2]
                    + "]}";
        }

        private Pet(String species, String nickname) {
            this.species = species;
            this.nickname = nickname;
        }

        private Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
            this.species = species;
            this.nickname = nickname;
            this.age = age;
            this.trickLevel = trickLevel;
            this.habits = habits;
        }

        private Pet() {
        }

        public String getSpecies() {
            return species;
        }

        public String getNickname() {
            return nickname;
        }

        public int getAge() {
            return age;
        }

        public int getTrickLevel() {
            return trickLevel;
        }

        public String[] getHabits() {
            return habits;
        }

        public void setSpecies(String species) {
            this.species = species;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public void setTrickLevel(int trickLevel) {
            this.trickLevel = trickLevel;
        }

        public void setHabits(String[] habits) {
            this.habits = habits;
        }
    }

    public static class Human {
        private String name = "Stefan";
        private String surname = "Salvatore";
        private int year = 1993;
        private int iq = 80;
        private Pet pet = new Pet();
        Family family;

        String[][] schedule = new String[7][2];

        private void greetPet() {
            System.out.println("Привет, " + pet.nickname);
        }

        private void describePet() {
            if(pet.trickLevel > 50){
                System.out.println("У меня есть " + pet.species + ", ему " + pet.age + " лет, он очень хитрый");
            }else{
                System.out.println("У меня есть " + pet.species + ", ему " + pet.age + " лет, он почти не хитрый");
            }
        }
        @Override
        public String toString() {
            return "Human{name='" + name
                    + "', surname='" + surname
                    + "', year=" + year
                    + ", iq=" + iq
                    + ", schedule=[[" + schedule[0][0] + "," + schedule[0][1] + "],"
                    + schedule[1][0] + "," + schedule[1][1] + "],"
                    + schedule[2][0] + "," + schedule[2][1] + "],"
                    + schedule[3][0] + "," + schedule[3][1] + "],"
                    + schedule[4][0] + "," + schedule[4][1] + "],"
                    + schedule[5][0] + "," + schedule[5][1] + "],"
                    + schedule[6][0] + "," + schedule[6][1] + "]]},";
        }

        private Human(String name, String surname, int year) {
            this.name = name;
            this.surname = surname;
            this.year = year;
        }

        private Human(String name, String surname, int year, int iq, Pet pet, String[][] schedule) {
            this.name = name;
            this.surname = surname;
            this.year = year;
            this.iq = iq;
            this.pet = pet;
            this.schedule = schedule;
        }

        private Human() {
        }

        public String getName() {
            return name;
        }

        public String getSurname() {
            return surname;
        }

        public int getYear() {
            return year;
        }

        public int getIq() {
            return iq;
        }

        public Pet getPet() {
            return pet;
        }

        public String[][] getSchedule() {
            return schedule;
        }

        public Family getFamily() {
            return family;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setSurname(String surname) {
            this.surname = surname;
        }

        public void setYear(int year) {
            this.year = year;
        }

        public void setIq(int iq) {
            this.iq = iq;
        }

        public void setPet(Pet pet) {
            this.pet = pet;
        }

        public void setSchedule(String[][] schedule) {
            this.schedule = schedule;
        }

        public void setFamily(Family family) {
            this.family = family;
        }
    }

    public static class Family{
        Human mother;
        Human father;
        Human[] children;
        Pet pet;

        public Human getMother() {
            return mother;
        }

        public Human getFather() {
            return father;
        }

        public Human[] getChildren() {
            return children;
        }

        public Pet getPet() {
            return pet;
        }

        public void setMother(Human mother) {
            this.mother = mother;
        }

        public void setFather(Human father) {
            this.father = father;
        }

        public void setChildren(Human[] children) {
            this.children = children;
        }

        public void setPet(Pet pet) {
            this.pet = pet;
        }

        @Override
        public String toString() {
            System.out.print("Family{mother='" + mother
                    + "', father='" + father
                    + "', children='");
            for (Human child : children) {
                System.out.print(child + ",");
            }
            return "pet=" + pet.toString();
        }

        public Family(Human mother, Human father) {
            this.mother = mother;
            this.father = father;
        }

        public void addChild(Human human){
                Human[] newChildren = Arrays.copyOf(children, children.length + 1);
                newChildren[newChildren.length - 1]= human;
        }

        public void deleteChild(int index) {
            if (this.children.length <= index || index < 0) {
                return;
            }
            Human[] temp = new Human[this.children.length - 1];
            for (int i = 0; i < this.children.length; i++) {
                if (i < index) {
                    temp[i] = this.children[i];
                }
                if (i > index) {
                    temp[i - 1] = this.children[i];
                }
            }
            this.children = temp;
        }

        public int countFamily() {
            return 2 + children.length;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Family family = (Family) o;
            return Objects.equals(mother, family.mother) &&
                    Objects.equals(father, family.father) &&
                    Arrays.equals(children, family.children);
        }

        @Override
        public int hashCode() {
            int result = Objects.hash(mother, father);
            result = 31 * result + Arrays.hashCode(children);
            return result;
        }
    }

    public static void main(String[] args) {
        Human daniel = new Human();
        Human emma = new Human("Emma", "Watson", 1990);
        Human rupert = new Human("Rupert", "Grint", 1988);
        Human rosa = new Human("Rosa", "Grint", 2010);
        Pet mur = new Pet();
        Pet barsik = new Pet("cat", "Barsik");
        String[] habbitsOfDog = {"to bark", "to walk", "to sleep"};
        Pet richard = new Pet("dog", "Richard", 2, 30, habbitsOfDog);
        Human astoria = new Human("Astoria", "Greengrass", 1982);
        Human draco = new Human("Draco", "Malfoy", 1980);

        String[][] scheduleOfScorpius = new String[7][2];

        scheduleOfScorpius[0][0] = "sunday";
        scheduleOfScorpius[0][1] = "do home work";
        scheduleOfScorpius[1][0] = "monday";
        scheduleOfScorpius[1][1] = "go to courses";
        scheduleOfScorpius[2][0] = "tuesday";
        scheduleOfScorpius[2][1] = "shopping trip";
        scheduleOfScorpius[3][0] = "wednesday";
        scheduleOfScorpius[3][1] = "watch a film";
        scheduleOfScorpius[4][0] = "thursday";
        scheduleOfScorpius[4][1] = "cook eat";
        scheduleOfScorpius[5][0] = "friday";
        scheduleOfScorpius[5][1] = "meeting with friends";
        scheduleOfScorpius[6][0] = "saturday";
        scheduleOfScorpius[6][1] = "apartment cleaning";

        Human scorpius = new Human("Scorpius", "Malfoy", 2005, 90, richard, scheduleOfScorpius);

        System.out.println(daniel.toString());
        System.out.println(emma.toString());
        System.out.println(rupert.toString());
        System.out.println(rosa.toString());
        System.out.println(astoria.toString());
        System.out.println(draco.toString());
        System.out.println(scorpius.toString());
        scorpius.greetPet();
        scorpius.describePet();
        System.out.println(scorpius.pet.toString());
    }
}